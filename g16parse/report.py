"""
Helper module for creating calculation reports.
"""

from g16parse import g16proc
import numpy as np

def report_items(log_file_path, keep_rot_trans):
    """
    Gather reports from a log file and put them in a dictionary.

    inputs:
    log_file_path: str/Path object, path to the .log file
    keep_rot_trans: bool, whether to keep translational and rotational contributions to E and S. Should be True for unconstrained calculations and False for constrained calculations.

    returns:
    dictionary of calculation results.
    """

    results = g16proc.OFProc(str(log_file_path))

    # Start collecting results
    completed = results.normal_termination()
    opt_coords = results.optimized_coords()	
    opt_coords = opt_coords.tolist()
    atomList = results.atom_list()

    for i in range(0, results.N):
        opt_coords[i].insert(0, atomList[i])

    NImag = results.number_imaginary()
    E_energy = results.electronic_E()
    spin_annihilation = results.spin_annihilation()
    convergences = results.convergence()
    freqs = np.array(results.frequencies())
    imag_freqs = freqs[freqs<0]

    # RT in Hartrees 
    R = 8.314/2625.499/1000
    T = results.temperature()

    if keep_rot_trans:
        G = results.free_energy()
        H = results.enthalpy()
        TS = H - G
    else:
        freqs = np.array(results.frequencies())
        imag_freqs = freqs[freqs<0]

        vib_temp = np.array(results.frequencies()) * 1.4387772494045046 # Value converts from 1/cm to K
        vib_temp = vib_temp[vib_temp>0]
        # IMPORTANT: this E_v definition has the ZPE built in (0.5 * vib_temp)
        E_v = R * np.sum(vib_temp * (0.5 + 1/(np.exp(vib_temp/T) - 1)))
        S_v = R * np.sum( (vib_temp/T) / (np.exp(vib_temp/T) - 1) - np.log(1 - np.exp(-vib_temp/T)))

        # electronic contribution to S
        S_e = R * results.log_q(2)

        H = E_energy + E_v + R*T
        S = S_v + S_e

        TS = T * S
        # Partition functions
        ln_q_trans = results.log_q(3)
        ln_q_rot = results.log_q(4)

        # Correction factor for removing rotational and translational contributions to E and S
        G = results.free_energy() + R * results.temperature() * (1 + ln_q_rot + ln_q_trans)

    g16results = {"Successful Job completion" :  completed,
        "Keep translations and rotations": keep_rot_trans,
        "Stationary Point Coordinates" :  opt_coords,
        "# Imaginary Frequencies" :  NImag,
        "Imaginary Frequencies (cm**-1)": imag_freqs.tolist(),
        "Electronic Energy (Ha)" :  E_energy,
        "Convergences": convergences,
        "S2 and S2A" :  spin_annihilation,
        "G (Ha)" :  G,
        "H (Ha)" : H,
        "TS (Ha)" : TS}

    # The dictionary results get nested one final time
    return g16results

def print_summary(g16_results, name):
    """
    Print out a summary of the calculation results:

    inputs:
    g16_results: dict, results dictionary from report_items().
    name: name of the the .log file
    """
    print("#### CALCULATION SUMMARY FOR {} ####".format(name))
    print('Successful Job completion: {}'.format(g16_results['Successful Job completion']))
    print("Keep translations and rotations: {}\n".format(g16_results["Keep translations and rotations"]))
    print('Final convergences (Value / Threshold):')
    for _, (conv_type, val_dict) in enumerate(g16_results["Convergences"].items()):
        for _, (max_or_rms, vals) in enumerate(val_dict.items()):
            print('  - {} {}: {} {:1.2E}/{:1.2E}'.format(
                max_or_rms, 
                conv_type, 
                vals['Converged?'], 
                vals['Value'], 
                vals['Threshold']))
    
    print('\nThermochemistry:')
    print('  - E (Ha): ', g16_results["Electronic Energy (Ha)"])
    print('  - H (Ha): ', g16_results['H (Ha)'])
    print('  - G (Ha): ', g16_results['G (Ha)'])
    print('  - TS (Ha): {:2.6f}\n'.format(g16_results['TS (Ha)']))
    print('Number of imaginary frequencies: {}'.format(g16_results['# Imaginary Frequencies']))

    if g16_results['# Imaginary Frequencies'] > 0:
        imag_freqs = g16_results["Imaginary Frequencies (cm**-1)"]

        imag_freq_str = ', '.join(['{:6.1f}i'.format(-x) for x in imag_freqs])
        print('Imaginary frequency value(s): {}'.format(imag_freq_str))

    pass