import re
import numpy as np
import linecache


class OFProc:
    """Class to parse values from the output file"""
    def __init__(self, output_file):
        self.output_file = output_file
        self.N = OFProc.set_N(self)
    
    def set_N(self):
        lookup =" NAtoms"
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if lookup in line:
                    location = num
                    break
            f.seek(0)
            Nlist = f.readlines()[location-1]
        nums = re.findall(r'\d+', Nlist)
        return int(nums[0])

    def optimized_coords(self):
        """
        Searches the output file (outputFileName) and extracts the x,y,z positions of the optimized coordinates as a np array
        """
        #Search string to find location of optimized coordinates
        lookup = " Number     Number       Type             X           Y           Z"
        #Open file, search for lookup, get line number
        location = OFProc.find_location(self, lookup, False) + 1

        with open(self.output_file) as f:
            coords = f.readlines()[location:location+self.N]
        # Extract all numbers
        nums = [re.findall(r"[-+]?\d*\.\d+|\d+", x) for x in coords]
        # Convert to np array
        opt_coords = np.array(nums).astype(np.float)

        # Return just the xyz positions
        return opt_coords[:, 3:]
        
    def atom_list(self):
        """
        Grabs the list of atoms from the output file. Returns as a list of strings
        """
        lookup =  " Number     Number       Type             X           Y           Z"
        location = OFProc.find_location(self, lookup, True) + 1

        with open(self.output_file) as f:
            coords = f.readlines()[location:location+self.N]
        # Extract all numbers
        nums = [re.findall(r"[-+]?\d*\.\d+|\d+", x) for x in coords]
        # Convert to np array of ints
        atom_list = np.array(nums)[:,1].astype(np.int)
        # a dictionary converting atomic number to element. Only goes up to Radon.
        periodic_table_of_elements = {1 : "H ", 2  : "He", 3  : "Li", 4  : "Be", 5  : "Be", 6  : "C ", 7  : "N ", 8  : "O ", 9  : "F ", 10 : "Ne", 11 : "Na", 12 : "Mg", 13 : "Al", 14 : "Si", 15 : "P ", 16 : "Si", 17 : "Cl", 18 : "Ar", 19 : "K ", 20 : "Ca", 21 : "Sc", 22 : "Ti", 23 : "V ", 24 : "Cr", 25 : "Mn", 26 : "Fe", 27 : "Co", 28 : "Ni", 29 : "Cu", 30 : "Zn", 31 : "Ga", 32 : "Ge", 33 : "As", 34 : "Se", 35 : "Br", 36 : "Kr", 37 : "Rb", 38 : "Sr", 39 : "Y ", 40 : "Zr", 41 : "Nb", 42 : "Mo", 43 : "Tc", 44 : "Ru", 45 : "Rh", 46 : "Pd", 47 : "Ag", 48 : "Cd", 49 : "In", 50 : "Sn", 51 : "Sb", 52 : "Te", 53 : "In", 54 : "Xe",  55 : "Cs", 56 : "Ba", 57 : "La", 58 : "Ce", 59 : "Pr", 60 : "Nd", 61 : "Pm", 62 : "Sm", 63 : "Eu", 64 : "Gd", 65 : "Tb", 66 : "Dy", 67 : "Ho", 68 : "Er", 69 : "Tm", 70 : "Yb", 71 : "Lu", 72 : "Hf", 73 : "Ta", 74 : "W ", 75 : "Re", 76 : "Os", 77 : "Ir", 78 : "Pt", 79 : "Au", 80 : "Hg", 81 : "Tl", 82 : "Pb", 83 : "Bi", 84 : "Po", 85 : "At", 86 : "Rn"}
        atom_list = [periodic_table_of_elements[atom_list[i]] for i in range(len(atom_list))]

        return atom_list

    def initial_coords(self):
        """
        Grabs the initial Cartesian coordinates from the output file.
        This method is particularly useful when the input file is in internal coordinates,
        but you want the Cartesian coordinates
        """
        lookup =  " Number     Number       Type             X           Y           Z"
        location = OFProc.find_location(self, lookup, True) + 1

        with open(self.output_file) as f:
            coords = f.readlines()[location:location+self.N]
        # Extract all numbers
        nums = [re.findall(r"[-+]?\d*\.\d+|\d+", x) for x in coords]
        # Convert to np array
        initial_coords = np.array(nums).astype(np.float)
        return initial_coords[:, 3:]

    def find_location(self, lookup, firstInstance):
        """
        Returns the 0 indexed location for a line in the output file
        Inputs: lookup: string. phrase to search for
                firstInstance: boolean. If true: find location of the first instance
                                        If false: find location of last instance
        Location of -1 means phrase is not found
        """
        location = -1
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if lookup in line:
                    location = num
                    if firstInstance is True:
                        break
        return location

    def electronic_E(self):
        """
        Retruns the electronic energy in Ha
        """
        location = -1
        lookup = ' SCF Done:'
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if lookup in line[0:10]:
                    location = num
            f.seek(0)
            readLine = f.readlines()[location-1]
        readLine = readLine.split()
        zeroPtEnergy = float(readLine[4])
        return zeroPtEnergy

    def mod_redundant_coords(self):
        """
        Read in all the modredundant coordinates as a list of string.
        A value of -1 means no modredundant coordinates have been detected
        """
        lookup = ' The following ModRedundant input section has been read:'
        location = OFProc.find_location(self, lookup, True)
        with open(self.output_file) as f:
            if location != -1:
                i = 0
                MRCoords = []
                # read in modredundant coords until theres no more (entry is NAtoms=)
                while True:
                    f.seek(0)
                    readLine = f.readlines()[location+i]
                    MRCoords.append(readLine.split())
                    # For g09 C01, Last line is NAtoms
                    # For g09 D01, last line is blank
                    if not MRCoords[i]:
                        # 'NAtoms=' is the 2nd line that follows the last modredundant entry. The 1st line is an empty space
                        # so delete it
                        del MRCoords[-1]
                        break
                    elif MRCoords[i][0] == 'NAtoms=':
                        del MRCoords[-1]
                        break
                    i += 1
            else:
                MRCoords = 0
        return MRCoords

    def spin_annihilation(self):
        """
        Returns the spin eigenvalue before and after annihilation. -1 implies closed shell
        """
        lookup = ' Annihilation of the first spin contaminant:'
        location = OFProc.find_location(self, lookup, False)
        if location != -1:
            with open(self.output_file) as f:
                f.seek(0)
                readLine = f.readlines()[location]
            readLine = readLine.split()
            s2 = float(readLine[3][0:-1])
            s2a = float(readLine[5])
        else:
            s2 = -1
            s2a = -1
        return s2, s2a

    def temperature(self):
        """
        returns temperature from frequency calculation, even if temperature has not been given as an input (gives default value then = 298.15)
        a value of -1 indicates no freq calc
        """
        lookup = '- Thermochemistry -'
        line_num = -1
        split_line = []
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if lookup in line:
                    line_num = num + 2
                    break
        if line_num == -1:
            temperature = -1
        else:
            line = linecache.getline(self.output_file, line_num)
            split_line.append(line.split())
            temperature = float(split_line[0][1])
        return temperature

    def free_energy(self):
        """
        Returns the free energy after a frequency calculation
        A value of -1 means the free energy was not found (no freq calc)
        """
        lookup = ' Sum of electronic and thermal Free Energies='
        location = OFProc.find_location(self, lookup, False)
        with open(self.output_file) as f:
            Elist = f.readlines()[location-1]

        # Check if free energy exists (i.e. there was an actual freq calculation)
        if location != -1:
            freeE = re.findall(r"[-+]?\d*\.\d+|\d+", Elist)
            freeE = float(freeE[0])
        else:
            freeE = -1
        return freeE

    def enthalpy(self):
        """
        Returns the enthalpy after a frequency calculation
        A value of -1 means the enthalpy was not found (no freq calc)
        """
        lookup = ' Sum of electronic and thermal Enthalpies='
        location = OFProc.find_location(self, lookup, False)
        with open(self.output_file) as f:
            H_list = f.readlines()[location-1]

        # Check if the enthalpy exists (i.e. there was an actual freq calculation)
        if location != -1:
            enthalpy = re.findall(r"[-+]?\d*\.\d+|\d+", H_list)
            enthalpy = float(enthalpy[0])
        else:
            enthalpy = -1
        return enthalpy

    def log_q(self, type=int):
        """
        Input: integer from 1-5.
        Returns the natural log of different components of the partition function \n
        type = 1 returns total (bot)\n
        type = 2 returns electronic\n
        type = 3 returns translational\n
        type = 4 returns rotational\n
        type = 5 returns vibrational\n
        """

        index = 0
        line_plus = 0
        split_line = []

        if type == 1:
            lookup = 'Total Bot'
            index = 1
        if type == 2:
            lookup = 'Electronic'
        if type == 3:
            lookup = 'Translational'
        if type == 4:
            lookup = 'Rotational'
        if type == 5:
            lookup = 'Total Bot'
            index = 1
            line_plus = 2

        with open(self.output_file) as myFile:
            for num, line in enumerate(myFile, 1):
                if lookup in line:
                    line_num = num + line_plus

        line = linecache.getline(self.output_file, line_num)
        split_line.append(line.split())
        LnQ = split_line[0][3 + index]
        return float(LnQ)

    def number_imaginary(self):
        """
        Returns the number of imaginary frequencies.\n
        -1 indicates that a frequency calculation was not performed
        """
        location = -1
        with open(self.output_file) as f:
            # Read in the whole file b/c there's no great way otherwise
            line = f.read()
        # Gets rid of any possibility of \NImag being on a newline and everything breaking
        line = line.replace('\n','')
        line = line.replace(' ','')
        location = line.find(r'\NImag')
        if location == -1:
            noImFreq = -1
        else:
            noImFreq = int(line[location+7])
        # Clear line since it reads in the whole damn file
        del line
        return noImFreq

    def masses(self):
        """
        Returns the masses in amu as a list
        """
        lookup = ' - Thermochemistry -'
        line_num = -1
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if lookup in line:
                    line_num = num + 2
                    break
            f.seek(0)
            lines = f.readlines()[line_num:line_num+self.N]

        lines = map(str.strip, lines)
        # Split up each row into list
        lines = list(map(lambda x: x.split(), lines))
        masses=[]

        for x in range(0,self.N):
            try:
                masses.append( float(lines[x][-1]))
            # If the masses are super heavy they throw this string, so replace this with a really big number
            except ValueError:
                if lines[x][-1]=='mass**********':

                    masses.append(100000000.)
            # if masses[x] == 0:
            #     masses[x] =float(IFProc.getNoHeavyAtoms()[1])
        return masses

    def frequencies(self):
        """Returns the IR frequencies in cm**-1 as a list """
        lookup = ' Frequencies --'
        loc = OFProc.find_location(self, lookup, True)-1
        lines=''
        freqList = []
        x=0
        with open(self.output_file) as f:
            f.seek(0)
            while lines != ' - Thermochemistry -\n':
                lines = f.readlines()[x+loc]
                if lines[0:15] == ' Frequencies --':
                    # Remove 'Frequencies --'
                    lines = lines[16:-1]
                    freqList.append(lines)
                f.seek(0)
                x +=1
        # String processing #
        # Strip \n
        freqList = map(str.strip, freqList)
        # Split
        freqList = map(lambda x: x.split(), freqList)
        # Flatten
        freqList = [y for x in freqList for y in x]
        # Cast as floats
        freqList = [float(w) for w in freqList]
        return freqList

    def normal_termination(self):
        """
        returns 1 if the log file has terminated normally, otherwise returns 0
        """
        split_line = []
        number_lines = sum(1 for line in open(self.output_file))
        line = linecache.getline(self.output_file, number_lines)
        split_line.append(line.split())
        if(split_line[0][0]=="Normal"):
            normal = True
        else:
            normal = False
        return normal

    def simulation_time(self):
        """
        Returns a list of how long each simulation takes in hrs. For single opt or freq calculations, a one element list will be returned
        For jobs with opt+freq, a two element list will be returned, the first element a 
        """
        job_time_string = []
        # Lookup string for g09, need to divide by # of cpu's for real time
        time_lookup = ' Job cpu time:'
        line_num = -1

        # First get the job cpu time
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if time_lookup in line:
                    job_time_string.append(line)
                    pass

        # Extracts floats from line saying how long job takes 
        # Example format is: 0 days  3 hours 21 minutes  2.8 seconds.
        # List will probably either be 1 x 4 or 2 x 4
        time = np.asarray([list(map(float, re.findall(r"[-+]?\d*\.\d+|\d+", job_time_string[x]))) for x in range(len(job_time_string))])

        cpu_lookup = ' %nprocshared='
        with open(self.output_file) as f:
            for num, line in enumerate(f, 1):
                if cpu_lookup in line:
                    n_cpus = int(re.search(r'\d+', line).group())
                    pass

        time[:,0] = time[:,0]*24        
        time[:,2] = time[:,2]/60
        time[:,3] = time[:,3]/3600

        return np.sum(time)/n_cpus

    def convergence(self):
        lookup = '         Item               Value     Threshold  Converged?'
        location = self.find_location(lookup, False)
        if location != -1:
            with open(self.output_file) as f:
                f.seek(0)
                readLine = f.readlines()[location:location+4]
            readLine = [x.split() for x in readLine]
        convergences = {
            "Forces": {
                "Max": {
                    "Value": float(readLine[0][2]),
                    "Threshold": float(readLine[0][3]),
                    "Converged?": bool(readLine[0][4])
                },
                "RMS":{
                    "Value": float(readLine[1][2]),
                    "Threshold": float(readLine[1][3]),
                    "Converged?": bool(readLine[1][4])
                }  
            },
            "Displacement": {
                "Max": {
                    "Value": float(readLine[2][2]),
                    "Threshold": float(readLine[2][3]),
                    "Converged?": bool(readLine[2][4])
                },
                "RMS":{
                    "Value": float(readLine[3][2]),
                    "Threshold": float(readLine[3][3]),
                    "Converged?": bool(readLine[3][4])
                }                  
            }
        }
        return convergences

class Thermochem(OFProc):
    """Extended OFProc class to perform constrained thermochemistry calculations"""
    def __init__(self, output_file, Hessian_file, n_Frozen=None):
        OFProc.__init__(self, output_file)
        Thermochem.Hessian_file = Hessian_file
        Thermochem.Hessian = Thermochem.load_Hessian(self)
        if n_Frozen == None:
            Thermochem.N_freeze = Thermochem.get_n_freeze(self)[0]
        else:
            Thermochem.N_freeze = n_Frozen
    
    def load_Hessian(self):
        """
        Returns the Cartesian Hessian (Ha/Bohr**2) as a numpy array, is read from fort.7
        \n
        fort.7 *must* be renamed to match the inputfile and output file.
        """
        with open(self.Hessian_file) as f:
            elements = f.readlines()
        # Strip \n
        elements = map(str.strip, elements)
        # Split up each row into list
        elements = map(lambda x: x.split(), elements)
        # Flatten list of list
        elements = [y for x in elements for y in x]
        # Replace D with e
        elements = [w.replace('D', 'e') for w in elements]
        # Cast as floats
        elements = [float(w) for w in elements]
        # Remove first 3N elements, these are not part of the Hessian
        del elements[0:3 * self.N]
        # Size of LT matrix based on number of elements
        arrSize = int(-0.5 + (np.sqrt(1 + 8 * len(elements)) / 2))

        # List -> LT matrix
        indices = np.tril_indices(arrSize)
        H = np.zeros((arrSize, arrSize))
        H[indices] = elements

        # Matrix is symmetric so fill it out
        H = H + np.transpose(H) - np.diag(np.diag(H))
        return H

    def load_Hessian2(self):

    	""" **DEPRECATED!!!!**
    	It is better to read the Hessian in from the fort.7 file obtained with the punch(derivatives) keyword
    	It has more digits and the Hessian doesn't always output for large atom jobs in the output file.
    	returns hessian as a numpy array.
    	type = "internal" or "Cartesian"
    	"""
    	f = open(self.output_file, "r")
    	hessian_data = []
    	for line in f:
    		if "Force constants in " + type + " coordinates:" in line:
    			for line in f:
    				if "Leave Link" in line:
    					break
    				line1 = line.replace("D", "E")
    				hessian_data.append(line1.split())
    	f.close()
    	n = 3*self.N
    	o = 5  # number of columns that gaussian prints ( current version = 5) , not checked for smaller number of coordinates (rare)
    	# variables used to parse through data
    	m = 0
    	p = self.N / o + 1
    	temp = 0
    	q = 0

    	hessian = np.zeros(shape=(n, n))


    	for i in range(0, n):
    		p = i / o
    		if p < 2:
    			q = 0
    		else:
    			q = p - 1
    		if m % o == 0:
    			temp = temp + q * o
    		for j in range(m, n):

    			hessian[j][m] = float(hessian_data[(n) * p - temp + p + j + 1 - p * o][m + 1 - o * p])
    			hessian[m][j] = hessian[j][m]
    		m = m + 1

    	return hessian
    def mod_redundant_list(self):
        """
        Read in all the modredundant coordinates as a list of string.
        A value of -1 means no modredundant coordinates have been detected
        """
        lookup = ' The following ModRedundant input section has been read:'
        location = OFProc.find_location(self, lookup, True)
        with open(self.output_file) as f:
            if location != -1:
                i = 0
                MRCoords = []
                # read in modredundant coords until theres no more (entry is NAtoms=)
                while True:
                    f.seek(0)
                    readLine = f.readlines()[location+i]
                    MRCoords.append(readLine.split())
                    # For g09 C01, Last line is NAtoms
                    # For g09 D01, last line is blank
                    if not MRCoords[i]:
                        # 'NAtoms=' is the 2nd line that follows the last modredundant entry. The 1st line is an empty space
                        # so delete it
                        del MRCoords[-1]
                        break
                    elif MRCoords[i][0] == 'NAtoms=':
                        del MRCoords[-1]
                        break
                    i += 1
            else:
                MRCoords = -1
        return MRCoords

    def get_n_freeze(self):
        N_Freeze = 0
        #	See if theres any MR coordinates
        MR = Thermochem.mod_redundant_list(self)
        if MR != -1:
            frozenAtomList = []
            for i in range(len(MR)):
                if MR[i][2] == 'F':
                    frozenAtomList.append(str(MR[i][1]))
                # Frozen Bond Length (B)
                try:
                    if MR[i][3] == 'F':
                        frozenAtomList.append(str(MR[i][1]))
                        frozenAtomList.append(str(MR[i][2]))
                except IndexError:
                    pass
                # Frozen Angle (A)
                try:
                    if MR[i][4] == 'F':
                        frozenAtomList.append(str(MR[i][1]))
                        frozenAtomList.append(str(MR[i][2]))
                        frozenAtomList.append(str(MR[i][3]))
                # Frozen Dihedral (D)
                except IndexError:
                    pass
                try:
                    if MR[i][5] == 'F':
                        frozenAtomList.append(str(MR[i][1]))
                        frozenAtomList.append(str(MR[i][2]))
                        frozenAtomList.append(str(MR[i][3]))
                        frozenAtomList.append(str(MR[i][4]))
                except IndexError:
                    pass
            frozenAtomList = list(set(frozenAtomList))
            try:
                frozenAtomList.remove('*')
            except ValueError:
                pass
            N_Freeze = len(frozenAtomList)
        else:
            N_Freeze = 0
            frozenAtomList = None

        return N_Freeze,frozenAtomList

    def constrained_frequencies(self, fixed_at_bottom=bool):
        """
        Inputs: keep_top_left, a boolean describing how to carve the Hessian. True keeps the top left block matrix and removes the bottom right (peripheral atoms at bottom). False keeps the bottom right block matrix and removes the top left (peripheral atoms at top).\n
        Performs a partial Hessian vibrational analysis for a constrained system a la Li and Jensen, 
        Theoretical Chemsitry Acc 2002. **H** is split into block matrix form where the peripheral atoms
        are removed.
        """
        unfrozen = self.N - self.N_freeze
        # Set off diagonal blocks to 0. Epsilon is a temporary matrix
        epsilon = np.diag(10. ** (-12) * np.ones(3 * self.N_freeze))
        # Zero array [3*N_Freeze x 3 * N_unfrozen] 
        z1 = np.zeros((3*self.N_freeze,3*unfrozen))
        # Zero array [3 * N_unfrozen x 3*N_Freeze] 
        z2 = np.transpose(z1)
        if fixed_at_bottom is True:
            # Carved out Hessian in block notation, eqn 11
            H = np.block([
                [self.Hessian[0:3 * unfrozen, 0:3 * unfrozen], z2     ],
                [z1                                          , epsilon]]) 
        else:
            # Carved out Hessian in block notation, eqn 11 with diagonals flipped
            H = np.block([[epsilon,                                               z1], 
                          [z2     , self.Hessian[3*self.N_freeze:, 3*self.N_freeze:]]])
    
        # Conversion factor from Ha/bohr**2 to amu/s**2
        convFactor = 4.3597439e-18 / (0.5291772086e-10) ** 2 / 1.66053878e-27
        H = H * convFactor
        # Get masses in amu
        masses = self.masses()

        # Vector of N masses to 3N masses
        masses = [x for pair in zip(masses, masses, masses) for x in pair]
        MminusHalf = np.diag(np.power(masses, -0.5))

        # Mass weight matrix
        H = np.dot(np.dot(MminusHalf, H), MminusHalf)
        # Get the eigenvalues
        evals = np.linalg.eigvals(H)

        # convert to wavenumbers
        allFreq = [np.sign(w) * np.sqrt(np.abs(w) / (4. * 3.14159 ** 2 * (2.99792458e10) ** 2)) for w in evals]
        signList = np.sign(allFreq)
        # Sort by magnitude so the smallest freq's get put to the bottom regardless of size
        # However, the permutations done by sorting also need to be carried over to the list of signs to preserve which are
        # imaginary
        sortedFreq, allSign = zip(*sorted(zip(np.abs(allFreq), signList)))
        allFreq = np.multiply(sortedFreq, allSign)

        # chemVib: 3n vibrational modes, where n is # of non constrained atoms
        
        # constraintFreq = allFreq[:3 * config.N_Freeze]
        chemVibs = allFreq[3 * self.N_freeze:]
        # chemRotTrans = np.zeros((6,1))

        # Lastly, count the new number of imaginary frequencies
        newNImag = 0
        for x in allSign:
            if x == -1:
                newNImag += 1

        return chemVibs, newNImag   
          
    def constrained_thermochem(self, fixed_at_bottom = bool, T = None, include_rot_trans_q=False):
        """
        Inputs: vibFreq - np array of vibrational frequencies from PHVA\n
        T - can specify a temperature different from the input file. Leaving this blank will use the temperature performed in Gaussian's calculation.
        Performs thermochem analysis ignoring constrained atoms.\n
        include_rot_trans_q=True includes rotational and translational partition function
        Returns G, H, TS, and the number of imginary freq discarded (should be 0 for min, 1 for TS)
        """
        vibFreqs = self.constrained_frequencies(fixed_at_bottom)[0]
        lenI = len(vibFreqs)
        # Remove imaginary (negative) frequencies

        freqList = [x for x in vibFreqs if x > 0]
        lenF = len(freqList)
        ImagFreqIgnored = lenF-lenI

        # Check if temperature is specified
        if T is None:
            T = self.temperature()

        vibTemp = [1.98644568E-25 * 100 / 1.38064852E-23 * w for w in freqList]
        R = 8.314
        kBT = T * 1.38064852E-23 / 4.35974465E-18
        # Calculate thermal corrections, ignoring partition functions from translation and rotation, and vibration of heavy atoms
        q_elec = self.log_q(2)
        if include_rot_trans_q is True:
            q_rot = self.log_q(4)
            q_trans = self.log_q(3)
        else:
            q_rot = 0
            q_trans = 0        

        # Translation
        St = (q_trans + 5/2.)*R 
        Et = 3/2. *R*T

        # Rotation
        Sr = (q_rot + 3 / 2.) * R
        Er = 3/2.*R*T

        # Electronic (both are 0)
        Se = R*q_elec
        Ee = 0

        # Vibrations. This formulation includes the ZPE
        Svk = [theta/T/(np.exp(theta/T)-1) - np.log(1-np.exp(-abs(theta)/T)) for theta in vibTemp]
        Evk = [theta * (0.5 + 1. / (np.exp(theta / T) - 1.)) for theta in vibTemp]

        Sv = R * sum(Svk)
        Ev = R * sum(Evk)
        # Thermal Corrections in Ha
        Ecorrection = (Et + Er + Ee + Ev) / 1000 / 2625.499638
        # Ha/K
        S = (St + Sr + Se + Sv) / 1000 / 2625.499638

        E = self.electronic_E()
        Ecorrected = E + Ecorrection

        constrainedH = Ecorrected + kBT
        constrainedTS = T * S
        constrainedG = constrainedH - constrainedTS
        return constrainedG, constrainedH, constrainedTS, ImagFreqIgnored

def main():

    pass

if __name__ == "__main__":
    main()