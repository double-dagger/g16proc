from setuptools import setup, find_packages

def readme():
    with open('README.md') as f:
        return f.read()

setup(name='g16parse',
      version='0.1',
      description='Parse .log file from g16 calculations',
      url='',
      author='Craig Vandervelden',
      author_email='craigv@illinois.edu',
      license='MIT',
      packages=['g16parse'],
      install_requires=['NumPy'],
      entry_points={
          'console_scripts': ['g16-report=g16parse.command_line:g16_report',
                              'g16-c-report=g16parse.command_line:g16_c_report']
      }
      )