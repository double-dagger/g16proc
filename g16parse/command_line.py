
import json
import argparse
from argparse import RawTextHelpFormatter
from pathlib import Path

from g16parse.report import report_items, print_summary

def g16_report():
    ap = argparse.ArgumentParser(description="""Generates a summary from an unconstrained DFT calculation in Gaussian16. Returns whether the calculation successfully terminated, optimized coordinates, energies, frequencies.
IMPORTANT: This script keeps rotational and translational contributions to E and S.""", formatter_class=RawTextHelpFormatter)

    # Arguments
    ap.add_argument("-p", "--path", required=True, type=str, help="Path to the .log file")
    ap.add_argument("-s", "--species", required=True, type=str, help="Species name to be used as the top key in json file")
    ap.add_argument('-v', '--verbose', help='Display verbose output. Will display file operations and a calculation summary.', action='count')

    args = vars(ap.parse_args())

    log_file_path = Path(args["path"]).resolve()
    species = args["species"]

    if args['verbose'] is None:
        verbose = False
    else:
        if args['verbose']:
            verbose = True
        else:
            verbose = False

    g16results = report_items(log_file_path, keep_rot_trans=True)

    # The dictionary results get nested one final time
    species_results = {species : g16results}

    if verbose:
        print('Writing results to {}\n'.format(log_file_path.with_suffix('.json')))

    with open(log_file_path.with_suffix('.json'), 'w') as outfile:
        json.dump(species_results, outfile, indent=2)

    if verbose:
        print_summary(g16results, log_file_path.name)

def g16_c_report():
    ap = argparse.ArgumentParser(description="""Generate a summary from a DFT calculation in Gaussian16 where atoms are constrained. Returns whether the calculation successfully terminated, optimized coordinates, energies, frequencies. 
IMPORTANT: This script removes rotational and translational contributions to E and S.""", formatter_class=RawTextHelpFormatter)

    # Arguments
    ap.add_argument("-p", "--path", required=True, type=str, help="Path to the .log file")
    ap.add_argument("-s", "--species", required=True, type=str, help="Species name to be used as the top key in json file")
    ap.add_argument('-v', '--verbose', help='Display verbose output. Will display file operations and a calculation summary.', action='count')

    args = vars(ap.parse_args())

    log_file_path = Path(args["path"]).resolve()
    species = args["species"]

    if args['verbose'] is None:
        verbose = False
    else:
        if args['verbose']:
            verbose = True
        else:
            verbose = False

    g16results = report_items(log_file_path, keep_rot_trans=False)

    # The dictionary results get nested one final time
    species_results = {species : g16results}
    if verbose:
        print('Writing results to {}\n'.format(log_file_path.with_suffix('.json')))
        
    with open(log_file_path.with_suffix('.json'), 'w') as outfile:
        json.dump(species_results, outfile, indent=2)

    if verbose:
        print_summary(g16results, log_file_path.name)